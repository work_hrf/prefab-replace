﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using System.IO;

[CustomEditor(typeof(UITemple))]
public class UITempleInspector : Editor
{

	//------------------------------------------------------------//

	//模板存放的路径
    private static string TEMPLE_PREFAB_PATH =  "Assets/UI/Temple";

    //Prefab存放的路径
    private static List<string> UIPrefabs = new List<string>()
    {
       "Assets/UI/Prefab"
    };

	//------------------------------------------------------------//




    [MenuItem("GameObject/UITemple/Creat To Prefab", false, 11)]
    static void CreatToPrefab(MenuCommand menuCommand)
    {
        if (menuCommand.context != null)
        {
			CreatDirectory();
            GameObject selectGameObject = menuCommand.context as GameObject;

            if (IsTemplePrefabInHierarchy(selectGameObject))
            {
                CreatPrefab(selectGameObject);
            }
            else
            {
                CreatPrefab(selectGameObject);
                GameObject.DestroyImmediate(selectGameObject);
            }
        }
        else
        {
            EditorUtility.DisplayDialog("错误！", "请选择一个GameObject", "OK");
        }
    }



    
    private UITemple uiTemple;

    void OnEnable()
    {
        uiTemple = (UITemple)target;
        if (IsTemplePrefabInInProjectView(uiTemple.gameObject))
        {
            ShowHierarchy();
        }
		CreatDirectory();
    }

    public override void OnInspectorGUI()
    {

 	    base.OnInspectorGUI();
	    bool isPrefabInProjectView = IsTemplePrefabInInProjectView(uiTemple.gameObject);
        EditorGUILayout.LabelField("GUID:" + uiTemple.GUID);
	
        GUILayout.BeginHorizontal();

		if (GUILayout.Button("Select"))
        {
			DirectoryInfo directiory = CreatDirectory();

            FileInfo[] infos = directiory.GetFiles("*.prefab", SearchOption.AllDirectories);
            for (int i = 0; i < infos.Length; i++)
            {
                FileInfo file = infos[i];
                GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>(file.FullName.Substring(file.FullName.IndexOf("Assets")));
                if(prefab.GetComponent<UITemple>().GUID == uiTemple.GUID)
                {
                    EditorGUIUtility.PingObject(prefab);
                    return;
                }
            }
        }
        
		if (isPrefabInProjectView)
        {

	        if (GUILayout.Button("Search"))
	        {
	            uiTemple.searPrefabs =  SearchPrefab(uiTemple.GUID);
				return;
	        }

	        if (GUILayout.Button("Apply"))
	        {
	            if (IsTemplePrefabInHierarchy(uiTemple.gameObject))
	            {

	                ApplyPrefab(uiTemple.gameObject,PrefabUtility.GetPrefabParent(uiTemple.gameObject), true);
	            }
	            else
	            {
	                

	               ApplyPrefab(uiTemple.gameObject, uiTemple.gameObject, false);
	            }
	            return;
	        }

	        if (GUILayout.Button("Delete"))
	        {
	            if (IsTemplePrefabInHierarchy(uiTemple.gameObject))
	            {
	                DeletePrefab(GetPrefabPath(uiTemple.gameObject));
	            }
	            else
	            {
	                DeletePrefab(AssetDatabase.GetAssetPath(uiTemple.gameObject));
	            }
				return;
	        }
		}
        GUILayout.EndHorizontal();


		if (isPrefabInProjectView)
		{
	        if(uiTemple != null && uiTemple.searPrefabs.Count > 0)
	        {
	            EditorGUILayout.LabelField("Prefab :" + uiTemple.name);

	            foreach (GameObject p in uiTemple.searPrefabs)
	            {
	                EditorGUILayout.Space();
	                if (GUILayout.Button(AssetDatabase.GetAssetPath(p))) {
	                    EditorGUIUtility.PingObject(p);
	                }
	            }
	        }
        }
    
    }

    static private List<GameObject> SearchPrefab(int guid)
    {
        List<GameObject> prefabs = new List<GameObject>();
        foreach(string forder in UIPrefabs)
        {
            DirectoryInfo directiory = new DirectoryInfo(Application.dataPath + "/" + forder.Replace("Assets/", ""));
            FileInfo[] infos = directiory.GetFiles("*.prefab", SearchOption.AllDirectories);
            for (int i = 0; i < infos.Length; i++)
            {
                FileInfo file = infos[i];
                GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>(file.FullName.Substring(file.FullName.IndexOf("Assets")));
                GameObject go = Instantiate<GameObject>(prefab);
                UITemple[] temples = go.GetComponentsInChildren<UITemple>();
                foreach (UITemple temple in temples)
                {
                   if(temple.GUID == guid && !prefabs.Contains(prefab))
                    {
                        prefabs.Add(prefab);
                    }
                }
                GameObject.DestroyImmediate(go);
            }
        }

        return prefabs;
    }

    static private  void ApplyPrefab(GameObject prefab, Object targetPrefab, bool replace)
    {
        if (EditorUtility.DisplayDialog("注意！", "是否进行递归查找批量替换模板？", "ok", "cancel"))
        {
            Debug.Log("ApplyPrefab : " + prefab.name );
            GameObject replacePrefab;
            int count = 0;
            if (replace)
            {
                PrefabUtility.ReplacePrefab(prefab, targetPrefab, ReplacePrefabOptions.ConnectToPrefab);
                Refresh();
                replacePrefab = targetPrefab as GameObject;
                count = prefab.GetComponentsInChildren<UITemple>().Length;
                
            }
            else
            {
                replacePrefab = AssetDatabase.LoadAssetAtPath<GameObject>(AssetDatabase.GetAssetPath(targetPrefab));
                GameObject checkPrefab = PrefabUtility.InstantiatePrefab(replacePrefab) as GameObject;
                count = checkPrefab.GetComponentsInChildren<UITemple>().Length;
                DestroyImmediate(checkPrefab);
            }


         
            if (count != 1)
            {
                EditorUtility.DisplayDialog("注意！", "无法批量替换，因为模板不支持嵌套。", "ok");
                return;
            }

            UITemple temple =  replacePrefab.GetComponent<UITemple>();

            if(temple != null)
            {
             
                List<GameObject> references = SearchPrefab(temple.GUID);
                foreach(GameObject reference in references)
                {
                    GameObject go = PrefabUtility.InstantiatePrefab(reference) as GameObject;
                    UITemple[] instanceTemples =  go.GetComponentsInChildren<UITemple>();
                    for(int i =0; i< instanceTemples.Length; i++ )
                    {
                        UITemple instance = instanceTemples[i];
                        if (instance.GUID == temple.GUID)
                        {
                            GameObject newInstance = GameObject.Instantiate<GameObject>(replacePrefab);
                            newInstance.name = replacePrefab.name;
                            newInstance.transform.SetParent(instance.transform.parent);
                            newInstance.transform.localPosition = instance.transform.localPosition;
                            DestroyImmediate(instance.gameObject);
                        }
                    }

                    PrefabUtility.ReplacePrefab(go, PrefabUtility.GetPrefabParent(go), ReplacePrefabOptions.ConnectToPrefab);
                    DestroyImmediate(go);
                }
            }
            ClearHierarchy();
            Refresh();
        }
    }



    static private void DeletePrefab(string path)
    {
        if (EditorUtility.DisplayDialog("注意！", "是否进行递归查找批量删除模板？", "ok", "cancel"))
        {
            Debug.Log("DeletePrefab : " + path);
            GameObject deletePrefab =  AssetDatabase.LoadAssetAtPath<GameObject>(path);
            UITemple temple = deletePrefab.GetComponent<UITemple>();
            if (temple != null)
            {
                List<GameObject> references = SearchPrefab(temple.GUID);
                foreach (GameObject reference in references)
                {
                    GameObject go = PrefabUtility.InstantiatePrefab(reference) as GameObject;
                    UITemple[] instanceTemples = go.GetComponentsInChildren<UITemple>();
                    for (int i = 0; i < instanceTemples.Length; i++)
                    {
                        UITemple instance = instanceTemples[i];
                        if (instance.GUID == temple.GUID)
                        {
                            DestroyImmediate(instance.gameObject);
                        }
                    }
                    PrefabUtility.ReplacePrefab(go, PrefabUtility.GetPrefabParent(go), ReplacePrefabOptions.ConnectToPrefab);
                    DestroyImmediate(go);
                }
            }
            AssetDatabase.DeleteAsset(path);
            ClearHierarchy();
            Refresh();
        }
    }


  

    static private void CreatPrefab(GameObject prefab)
    {

        string creatPath = TEMPLE_PREFAB_PATH + "/" + prefab.name + ".prefab";
        Debug.Log("CreatPrefab : " + creatPath);

        if (AssetDatabase.LoadAssetAtPath<GameObject> (creatPath) == null)
        {
            
            UITemple[] temps =  prefab.GetComponentsInChildren<UITemple>();

            for(int i =0; i< temps.Length; i++)
            {
                DestroyImmediate(temps[i]);
            }

            prefab.AddComponent<UITemple>().InitGUID();
            PrefabUtility.CreatePrefab(TEMPLE_PREFAB_PATH + "/" + prefab.name + ".prefab", prefab);
            Refresh();

        }
        else
        {
            EditorUtility.DisplayDialog("错误！", "Prefab名字重复，请重命名！", "OK");
        }
        
    }



    static private void Refresh()
    {
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        EditorApplication.SaveScene();
    }


    static  private void ClearHierarchy()
    {
        Canvas canvas = GameObject.FindObjectOfType<Canvas>();

        if (canvas != null)
        {
			for(int i =0; i < canvas.transform.childCount; i++){

				Transform t = canvas.transform.GetChild(i);
				if(t.GetComponent<UITemple>()!= null){
					GameObject.DestroyImmediate(t.gameObject);
				}

			}
        }
    }

    private void ShowHierarchy()
    {
       
        if (!IsTemplePrefabInHierarchy(uiTemple.gameObject))
        {
            Canvas canvas = GameObject.FindObjectOfType<Canvas>();
			if (canvas != null)
            {
				if((canvas.transform.childCount == 0) ||
				 (canvas.transform.childCount == 1 && canvas.transform.GetChild((0)).GetComponent<UITemple>() != null)){
					ClearHierarchy();
	                GameObject go = PrefabUtility.InstantiatePrefab(uiTemple.gameObject) as GameObject;
	                go.name = uiTemple.gameObject.name;

					GameObjectUtility.SetParentAndAlign(go, canvas.gameObject);
	                EditorGUIUtility.PingObject(go);
				}
             
            }

        }


    }

    static private bool IsTemplePrefabInHierarchy(GameObject go)
    {
        return (PrefabUtility.GetPrefabParent(go) != null);
    }

    static private bool IsTemplePrefabInInProjectView(GameObject go)
    {
        string path = AssetDatabase.GetAssetPath(go);
        if (!string.IsNullOrEmpty(path))
            return (path.Contains(TEMPLE_PREFAB_PATH));
        else
            return false;
    }

	static private DirectoryInfo CreatDirectory()
	{
		DirectoryInfo directiory = new DirectoryInfo(Application.dataPath + "/" + TEMPLE_PREFAB_PATH.Replace("Assets/",""));
		if(!directiory.Exists)
		{
			directiory.Create();
			Refresh();
		}
		return directiory;
   	}

    static private string GetPrefabPath(GameObject prefab)
    {
        Object prefabObj = PrefabUtility.GetPrefabParent(prefab);
        if (prefabObj != null)
        {
            return AssetDatabase.GetAssetPath(prefabObj);
        }
        return null;
    }

}
