# README #

提供批量替换Prefab里的Prefab.

首先打开UITempleInspector.cs 设置好模板生成的路径，然后是引用模板的路径（可以是多个路径）。 

//------------------------------------------------------------//

    //模板存放的路径 
    private static string TEMPLE_PREFAB_PATH =  "Assets/UI/Temple";

    //Prefab存放的路径
    private static List<string> UIPrefabs = new List<string>()
    {
       "Assets/UI/Prefab"
    };

//------------------------------------------------------------//

1.创建UI模板

![1.gif](https://bitbucket.org/repo/bb8MLG/images/263223329-1.gif)

2.将模板绑定在某个UI的Prefab上

![1.gif](https://bitbucket.org/repo/bb8MLG/images/1273480313-1.gif)

3.修改模板

由于后期的模板可能比较多，通过名子很难确定模板的样式，所以选中模板时会自动实例化预览。

![2.gif](https://bitbucket.org/repo/bb8MLG/images/3665892497-2.gif)

5.批量替换所有引用模板

![3.gif](https://bitbucket.org/repo/bb8MLG/images/3406570012-3.gif)

6.搜索模板的引用关系，这里可以列出这个模板被那些Prefab引用着

![4.gif](https://bitbucket.org/repo/bb8MLG/images/2678233696-4.gif)